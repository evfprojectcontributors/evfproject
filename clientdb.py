#!/usr/bin/python3
from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client['vulndb']
vuln = db.vulns
case = {}
#we are using mongoclient and assigning the database vulndb to the variabledb. We are then assigning the collection vulns to the variable vuln.
print("Which vulnerability do you want to implement(type the name exactly)?")
index = 1
for vulns in vuln.find():
 print(str(index) + "." + vulns["name"])
 index = index + 1
name = "none"
number = input("Enter the choice: ")
index = 1
for vulns in vuln.find():
 if(index == int(number)):
  name = vulns["name"]
 index = index + 1

vuln_dict = vuln.find_one({"name":name})  
#vuln_dict is the dictionary that contains the selected vulnerability

case['name'] = vuln_dict['name']
case['CVE'] = vuln_dict['CVE'] 

#So far we have let the user select which vulnerability they want to implement and enter the name of the vuln in our case DB dictionary

print("Which Operating system do you want to use?(Enter the number)") 
index = 1
for os in vuln_dict['OS_list']:
 print(str(index) + "." + os['os_name'])
 index = index + 1

number = input("Enter number:")
case['OS'] = vuln_dict['OS_list'][int(number) - 1]['os_name'] 

 #The user has selected which operating system he wants to use
print("The users in the vulnDB are as follows")
for users in vuln_dict['users']:
 print("----------------------------------")
 print("Username:" + users['username'] + "\n" + "Password:" + users['password'] + "\n" + "Sudoer-Status:" + users['sudoer-status'] )
 print("----------------------------------")
print("Do you want to keep these users?\n1.Keep users\n2.Append\n3.Create New")
choice =input("Enter choice:")
if int(choice)==1:
 caseuser=vuln_dict['users']
elif int(choice)==2:
 caseuser=vuln_dict['users']
 print("Enter non root user names in the following format.\n username:password:sudoer status(sudo/non-sudo)\n\n")
 userlist = input("users:  ").split(",")
 for user in userlist:
    user_details = user.split(":")
    if user_details != ['']:
      details = {'username' : user_details[0], 'password' : user_details[1], 'sudoer-status' : user_details[2]}
      caseuser.append(details)
      details.clear
elif int(choice)==3:
 caseuser=[]
 print("Enter non root user names in the following format.\n username:password:sudoer status(sudo/non-sudo)\n\n")
 userlist = input("users:  ").split(",")
 for user in userlist:
    user_details = user.split(":")
    if user_details != ['']:
      details = {'username' : user_details[0], 'password' : user_details[1], 'sudoer-status' : user_details[2]}
      caseuser.append(details)
      details.clear
case['users'] = caseuser
#users can either be copied from vuln db or we can add onto it or create a new list of users

program_list = []
program_details = {}
print("Select programs to install")
index = 1
for programs in vuln_dict['OS_list'][int(number) - 1]['details']['programs'] :
 for versions in programs['version']:
  print(str(index) + "." + programs['name'] + " " + versions)
  index+=1
 program_choice = int(input("Enter choice:"))
 program_details = {'name':programs['name'], 'version': programs['version'][program_choice - 1]}
 program_list.append(program_details)
 index = 1
case['programs'] = program_list

#We are allowing the user to select one version of each software that is required by the vulnerability
#There is a problem however. What if a particular version requires another particular version of another software

case['commands'] = vuln_dict['OS_list'][int(number) - 1]['details']['commands']
case['config_files'] = vuln_dict['OS_list'][int(number) - 1]['details']['config_files']
print("The case is as follows")
for k in case:
 print(k + "  : ", end="" )
 print(case[k])
choice = input("Enter into case database?[Y/N]")
if choice.lower() == 'y':
 new_case = db.cases
 post_id = new_case.insert_one(case).inserted_id
else:
 print("exiting")
 exit(0)





