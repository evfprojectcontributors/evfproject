#!/usr/bin/python3
import sys
from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client['vulndb']
#we are using mongoclient and assigning the database vulndb to the variabledb. We are then assigning the collection vulns to the variable vuln.

def add_new(name):
  for vulnerabilities in db.vulns.find():
   if name.lower() in vulnerabilities.values():
    print("Vulnerability already exists")
    exit(0)
   #If a vuln already exists then it gives a message and exits the program
  vuln = {'name' : name.lower()}
  print("\nLeave blank if no  such value exists for your vulnerability.\nSeparate values using commas if multiple values.\n")
  vuln['CVE'] = input("CVE:  ")
  OS = input("OS:  ")
  os_names = OS.split(",")
  os_list = []
  for operating_systems in os_names:
 #We need to ask for programs to be installed, commands to be run and config files for each Operating system
    single_OS = {"os_name" : operating_systems, "details" : {}}
    os_details={}
    print("-----------------------------------------------------------------------------------------------")
    print("You are now entering details for the " + operating_systems + " OS")
    print("Enter programs to be installed in the format name:version\nIf there are multiple versions then use | to separate them(bash:4.1|4.2|4.3) \n\n")
    program_list = input("Programs:  ").split(",")
    final_program_list = []
    for program in program_list:
      program_details = program.split(":")
      if program_details != ['']:
        version_list=program_details[1].split("|")
        details = {'name' : program_details[0], 'version' : version_list}
        final_program_list.append(details)
        details.clear
    os_details['programs'] = final_program_list

    print("Enter configuration files in the format source;destination;permissions\n")
    config_list = input("Configs:  ").split(",")
    final_config_list = []
    for config in config_list:
      config_details = config.split(";")
      if config_details != ['']:
        details = {'source' : config_details[0], 'destination' : config_details[1], 'permissions' : config_details[2]}
        final_config_list.append(details)
        details.clear
    os_details['config_files'] = final_config_list
  
    print("Enter any commands to be run in order(these will be run in the same order that you enter)\n")
    commands = input("Commands:  ").split(",")
    os_details['commands'] = commands
    single_OS['details'] = os_details
    os_list.append(single_OS)
  vuln['OS_list'] = os_list
  print("Enter non root user names in the following format.\n username:password:sudoer status(sudo/non-sudo)\n\n")
  userlist = input("users:  ").split(",")
  final_user_list = []
  for user in userlist:
    user_details = user.split(":")
    print(user_details)
    if user_details != ['']:
      details = {'username' : user_details[0], 'password' : user_details[1], 'sudoer-status' : user_details[2]}
      final_user_list.append(details)
      details.clear
  vuln['users'] = final_user_list

  
  print("Vulnerability created as follows")
  print(vuln)
  confirm = input("Insert into database?[Y/N]")
  if confirm.lower() == "y":
   new_vuln = db.vulns
   post_id = new_vuln.insert_one(vuln).inserted_id
  else:
   print("exiting")
   exit(0)

if len(sys.argv) != 2:
  print("evf-program options\n -a  add new vuln \n -e  edit vuln\n -d  delete vuln")
  exit(0)

if sys.argv[1] == "-a":
  name = input('Enter Vuln name:  ')
  add_new(name)
elif sys.argv[1] == "-e":
  print("functionality yet to be added")
elif sys.argv[1] == "-d":
  print("functionality yet to be added")
else:
  exit(0)

