#!/usr/bin/python3
from pymongo import MongoClient
from jinja2 import Environment, PackageLoader
import subprocess
#Connecting to our database
client = MongoClient('localhost', 27017)
db = client['vulndb']
vuln = db.cases
#vuln is our collection cases
#We load the environment from where we can load our templates
env = Environment(loader=PackageLoader('EVF', 'templates'))
#printing the available cases and asking the user to choose
index = 1
for vulns in vuln.find():
 print(str(index) + "." + vulns["name"])
 index = index + 1
name = "none"
number = input("Enter the choice: ")
index = 1
for vulns in vuln.find():
 if(index == int(number)):
  id = vulns["_id"]
 index = index + 1

case_dict = vuln.find_one({"_id":id})  
filename = str(id) + ".sls"
f = open(filename, 'w')
#case_dict is the dictionary that contains the selected case

#We will first parse the usernames and passwords and create the config
#for these items
for users in case_dict['users']:
 username = users['username']
 password = users['password']
 template = env.get_template('create-user')
 f.write(template.render(username = username, password = password))
#We are now parsing the programs to be installed and adding that to
#our config
for programs in case_dict['programs']:
 name = programs['name']
 version = programs['version']
 template = env.get_template('install_programs')
 f.write(template.render(name = name, version = version))
#Parsing the  config files to be placed in the machine
for config_files in case_dict['config_files']:
 source = config_files['source']
 destination = config_files['destination']
 permissions = config_files['permissions']
 template = env.get_template('configs')
 f.write(template.render(source = source, destination = destination, permission = permissions))

#The commands that have to be run on the machine
index = 1
for commands in case_dict['commands']:
 number = "Command " + str(index)
 template = env.get_template('commands')
 f.write(template.render(label = number, command = commands))
 index+=1
