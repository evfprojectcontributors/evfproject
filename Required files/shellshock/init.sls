download:
  cmd.run:
    - name : 'wget http://gnu.mirror.iweb.com/bash/bash-4.1.tar.gz'
untar:
  cmd.run:
    - name : 'tar -xvf bash-4.1.tar.gz'
  
configure:
  cmd.run:
    - name : 'bash-4.1/configure'
make:
  cmd.run:
#    - cwd : '/root/'
    - name : 'make'
makeinstall:
  cmd.run:
#    - cwd : '/root/'
    - name : 'make install'
#cleaningup:
#  cmd.run:
#    - name : 'rm -rf /root/bash*'

apache:
  pkg:
    - name: apache2
    - installed
  cmd.run:
    - name : 'a2enmod cgi'

/usr/lib/cgi-bin/index.sh:
  file.managed:
    - source : salt://shellshock/bashcgi.sh
    - mode : 755

label:
  cmd.run:
    - name : 'service apache2 restart'
