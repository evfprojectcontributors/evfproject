This set of scripts will allow you to create a vulnerability, build a case from
a vulnerability and then build salt state files from a case. 


What do we need for this to work?

We need to be running a mongo db server and create a database called vulndb or 
change the database name in the script(evf-program.py). 

How to create a new vulnerability?

You will need to use the script(evf-program.py)Be careful about using 
this script because it is very sensitive to the format. Read the instructions 
it provides very carefully. The script might need to be run as root.

How to create a new case?

You will need to run the script(clientdb.py) and make sure the database names
are consistent. If you have followed the default instructions it should be fine.
The script will give you the instructions to move forward.

How to deploy a case?

Use the script(deploy.py) to deploy a case. A salt state file(.sls) file should 
be created in the same directory. We can now use this file in salt. To run the 
script you need to have the EVF directory in the same directory as the script.
The directory which has deploy script should look like what is shown below at a 
minimum.

~
~deploy.py
~EVF
~EVF/templates
~EVF/templates/commands
~EVF/templates/configs
~EVF/templates/create-user
~EVF/templates/install_programs
~
~

Deploying the vulnerable VM using Salt

Let's look at an example on how to build a VM that is vulnerable to shellshock. 
The vulnerability database must have a shellshock vulnerability that should look
similar to the JSON given in the example. You can then use the clientdb script 
to create a case. Next we use the deploy script to generate the salt state file.

Our next step requires salt to be installed. We need to install salt master on 
the machine we are running. you can use the package manager to install salt-master.
This could be something along the lines of "apt-get install salt-master" or you can
also use other methods from their website(https://docs.saltstack.com/en/latest/topics/installation/index.html).

You can run salt-master using the command "salt-master -d". You need to follow similar 
instructions to install a salt minion on a different VM. Once the installation of the 
minion is complete, you need to edit its configuration to point it to the master.
The configuration file is located at /etc/salt/minion. Open this file and change the
value next to the field master. The default value should be salt but you can change it
to the IP address of the master. Make sure that the line isn't commented. Once this 
change has been saved you can choose to edit the ID of the minion by changing the value 
of the id field.

Start the salt-minion using the command "salt-minion -d". If everything was done right 
until this moment. Typing "salt-keys -L" on the salt master should show you the name
of the minion(May need root privileges to use salt commands). Type in "salt-keys -A"
to accept the minion. Test connectivity using the command "salt '*' test.ping".

Create the directory /srv/salt/shellshock. Copy the required files into the directory.
This should be the files 000-default.conf, bashcgi.sh and the bash tarball file.
Copy the salt state file created by the deploy script into this directory and rename it
to "init.sls". The directory should now look something like this.

/srv/salt/shellshock
~
~init.sls
~bash-4.1.tar.gz
~000-default.conf
~bashcgi.sh
~

Once this is done, type in "salt '*' state.apply shellshock". Alternately you can replace
the * with the name of the minion. This command should build the vulnerable VM.

